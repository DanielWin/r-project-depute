# R-Project-Depute

## Summary 

The main functions of this package are : 
<ul>
<li>__getPreprocessedDataFrameFromURL__ : it takes as input an URL of a given debate (e.g "https://www.nosdeputes.fr/15/seance/744/xml"), and outputs a clean dataframe which includes analysis of sentiment of the crowd.</li>
<li>__getDebatesFromMP__ : it takes as input the name of an MP (e.g. Damien Adam) and outputs the list of the debates he took part in.</li>
<li>__getMetricsFromMPDataFrame__ : it takes as input the name of an MP, the URL of the topic he took part in, and then outputs the metrics about his interventions in all the debates related to the topic</li>
<li>__dossierToDebates__ : it takes as input the URL of a topic, then outputs metrics of the different political groups who contributed to the debates of the topic</li>
<li>__getTopics__ : takes an integer _n_ and outputs the _n_ most recent topics</li>
</ul>
